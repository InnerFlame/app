﻿-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.38-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных application
CREATE DATABASE IF NOT EXISTS `application` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `application`;


-- Дамп структуры для таблица application.role_tbl
CREATE TABLE IF NOT EXISTS `role_tbl` (
  `id` int(11) DEFAULT NULL COMMENT 'идентификатор роли',
  `role_text` varchar(255) DEFAULT NULL COMMENT 'роль',
  `role_code` varchar(255) DEFAULT NULL COMMENT 'символьный код роли'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='справочная таблица ролей';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица application.user_hst
CREATE TABLE IF NOT EXISTS `user_hst` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор таблицы истории пользователей',
  `user` varchar(255) NOT NULL COMMENT 'пользователь, который изменил данные',
  `date_action` datetime NOT NULL COMMENT 'дата и время изменений',
  `action` varchar(255) NOT NULL COMMENT 'тип изменений',
  `old_email` varchar(255) DEFAULT NULL COMMENT 'старые данные поля email',
  `new_email` varchar(255) DEFAULT NULL COMMENT 'новые данные поля email',
  `old_password` varchar(255) DEFAULT NULL COMMENT 'старые данные поля password',
  `new_password` varchar(255) DEFAULT NULL COMMENT 'новые данные поля password',
  `old_first_name` varchar(255) DEFAULT NULL COMMENT 'старые данные поля имя',
  `new_first_name` varchar(255) DEFAULT NULL COMMENT 'новые данные поля имя',
  `old_second_name` varchar(255) DEFAULT NULL COMMENT 'старые данные поля фамилия',
  `new_second_name` varchar(255) DEFAULT NULL COMMENT 'новые данные поля фамилия',
  `old_third_name` varchar(255) DEFAULT NULL COMMENT 'старые данные поля отчество',
  `new_third_name` varchar(255) DEFAULT NULL COMMENT 'новые данные поля отчество',
  `old_office` varchar(255) DEFAULT NULL COMMENT 'старые данные поля должность',
  `new_office` varchar(255) DEFAULT NULL COMMENT 'новые данные поля должность',
  `old_role` int(11) DEFAULT NULL COMMENT 'старые данные поля роль',
  `new_role` int(11) DEFAULT NULL COMMENT 'новые данные поля роль',
  `old_department` int(11) DEFAULT NULL COMMENT 'старые данные поля отдел',
  `new_department` int(11) DEFAULT NULL COMMENT 'новые данные поля отдел',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='таблица истории пользователей';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица application.user_tbl
CREATE TABLE IF NOT EXISTS `user_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор пользователя',
  `email` varchar(255) DEFAULT NULL COMMENT 'електронная почта пользователя',
  `password` varchar(255) DEFAULT NULL COMMENT 'пароль пользователя md5',
  `first_name` varchar(255) DEFAULT NULL COMMENT 'имя  пользователя',
  `second_name` varchar(255) DEFAULT NULL COMMENT 'фамилия пользователя',
  `third_name` varchar(255) DEFAULT NULL COMMENT 'отчество пользователя',
  `office` varchar(255) DEFAULT NULL COMMENT 'должность пользователя',
  `role_id` int(11) DEFAULT NULL COMMENT 'роль пользователя',
  `department_id` int(11) DEFAULT NULL COMMENT 'идентификатор отдела  пользователя',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='справочная таблица пользователей';

-- Экспортируемые данные не выделены.


-- Дамп структуры для триггер application.user_tbl_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `user_tbl_after_insert` AFTER INSERT ON `user_tbl` FOR EACH ROW BEGIN
INSERT INTO application.user_hst(
		`id`
		, `user`
		, `date_action`
		, `action`			
		, `new_email`
		, `new_password`
		, `new_first_name`	
		, `new_second_name`
		, `new_third_name`
		, `new_office`
		, `new_role`
		, `new_department`	
		)
	VALUES (
		null
		, CURRENT_USER()
		, NOW()
		, 'INSERT'	
		, new.email
		, new.password
		, new.first_name
		, new.second_name
		, new.third_name
		, new.office
		, new.role_id
		, new.department_id);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Дамп структуры для триггер application.user_tbl_after_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `user_tbl_after_update` BEFORE UPDATE ON `user_tbl` FOR EACH ROW BEGIN
INSERT INTO application.user_hst(
		`id`
		, `user`
		, `date_action`
		, `action`			
		, `new_email`
		, `old_email`
		, `new_password`
		, `old_password`
		, `new_first_name`
		, `old_first_name`	
		, `new_second_name`
		, `old_second_name`
		, `new_third_name`
		, `old_third_name`
		, `new_office`
		, `old_office`
		, `new_role`
		, `old_role`
		, `new_department`
		, `old_department`)
	VALUES (
		null
		, CURRENT_USER()
		, NOW()
		, 'UPDATE'	
		, new.email
		, old.email
		, new.password
		, old.password
		, new.first_name
		, old.first_name
		, new.second_name
		, old.second_name
		, new.third_name
		, old.third_name
		, new.office
		, old.office
		, new.role_id
		, old.role_id
		, new.department_id
		, old.department_id);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Дамп структуры для триггер application.user_tbl_before_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `user_tbl_before_delete` BEFORE DELETE ON `user_tbl` FOR EACH ROW BEGIN
INSERT INTO application.user_hst(
		`id`
		, `user`
		, `date_action`
		, `action`			
		, `old_email`
		, `old_password`
		, `old_first_name`	
		, `old_second_name`
		, `old_third_name`
		, `old_office`
		, `old_role`
		, `old_department`)
	VALUES (
		null
		, CURRENT_USER()
		, NOW()
		, 'DELETE'	
		, old.email
		, old.password
		, old.first_name
		, old.second_name
		, old.third_name
		, old.office
		, old.role_id
		, old.department_id);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
