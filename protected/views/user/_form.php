<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'user-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        //'enableAjaxValidation'=>true,
    )); ?>

<!--    --><?php
//    foreach (Yii::app()->user->getFlashes() as $type=>$flash) {
//        echo "<div class='{$type}'>{$flash}</div>";
//    }
//    ?>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'second_name'); ?>
        <?php echo $form->textField($model,'second_name',array('size'=>60,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'second_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'third_name'); ?>
        <?php echo $form->textField($model,'third_name',array('size'=>60,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'third_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'office'); ?>
        <?php echo $form->textField($model,'office',array('size'=>60,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'office'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'confirmPassword'); ?>
        <?php echo $form->passwordField($model,'confirmPassword',array('size'=>60,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'confirmPassword'); ?>
    </div>



    <div class="row">
        <?php echo $form->labelEx($model,'role_id'); ?>
        <?php echo $form->passwordField($model,'role_id'); ?>
        <?php echo $form->error($model,'role_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'department_id'); ?>
        <?php echo $form->passwordField($model,'department_id'); ?>
        <?php echo $form->error($model,'department_id'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->