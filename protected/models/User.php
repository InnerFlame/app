<?php

/**
 * This is the model class for table "user_tbl".
 *
 * The followings are the available columns in table 'user_tbl':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $second_name
 * @property string $third_name
 * @property string $office
 * @property integer $role_id
 * @property integer $department_id
 */
class User extends CActiveRecord
{

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_BANNED = 'banned';

    public $confirmPassword;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_tbl';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('role_id, department_id', 'numerical', 'integerOnly'=>true),
			array('email, password, first_name, second_name, third_name, office', 'length', 'max'=>255),

                array('email', 'email'),
                array('confirmPassword', 'compare', 'compareAttribute'=>'password'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, first_name, second_name, third_name, office, role_id, department_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'first_name' => 'First Name',
			'second_name' => 'Second Name',
			'third_name' => 'Third Name',
			'office' => 'Office',
			'role_id' => 'Role',
			'department_id' => 'Department',
                'confirmPassword'=>'Confirm',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('second_name',$this->second_name,true);
		$criteria->compare('third_name',$this->third_name,true);
		$criteria->compare('office',$this->office,true);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('department_id',$this->department_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave()
    {

        if(empty($this->password) && empty($this->repeat_password) && !empty($this->initialPassword))
            $this->password=$this->repeat_password=$this->initialPassword;


        if(parent::beforeSave()) {

            $this->password = md5('innerflame' . $this->password);

            return true;
        }
        else
            return false;
    }
}
